package com.example.demo;

import static io.restassured.RestAssured.get;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

	@LocalServerPort
	private int port;

	@Test
	public void contextLoads() {
	}

	@Test
	public void test_0_0() {
		get("http://localhost:" + port + "/downloadTest/getFile_0_0").then().statusCode(200);
	}

	@Test
	public void test_0_1() {
		get("http://localhost:" + port + "/downloadTest/getFile_0_1").then().statusCode(200);
	}

	@Test
	public void test_0_2() {
		get("http://localhost:" + port + "/downloadTest/getFile_0_2").then().statusCode(200);
	}

	@Test
	public void test_0_3() {
		get("http://localhost:" + port + "/downloadTest/getFile_0_3").then().statusCode(200);
	}

	@Test
	public void test_1_0() {
		get("http://localhost:" + port + "/downloadTest/getFile_1_0").then().statusCode(200);
	}

	@Test
	public void test_1_1() {
		get("http://localhost:" + port + "/downloadTest/getFile_1_1").then().statusCode(200);
	}

	@Test
	public void test_1_2() {
		get("http://localhost:" + port + "/downloadTest/getFile_1_2").then().statusCode(200);
	}

	@Test
	public void test_1_3() {
		get("http://localhost:" + port + "/downloadTest/getFile_1_3").then().statusCode(200);
	}

	@Test
	public void test_2_0() {
		get("http://localhost:" + port + "/downloadTest/getFile_2_0").then().statusCode(200);
	}

	@Test
	public void test_2_1() {
		get("http://localhost:" + port + "/downloadTest/getFile_2_1").then().statusCode(200);
	}

	@Test
	public void test_2_2() {
		get("http://localhost:" + port + "/downloadTest/getFile_2_2").then().statusCode(200);
	}

	@Test
	public void test_2_3() {
		get("http://localhost:" + port + "/downloadTest/getFile_2_3").then().statusCode(200);
	}

	@Test
	public void test_3_0() {
		get("http://localhost:" + port + "/downloadTest/getFile_3_0").then().statusCode(200);
	}

	@Test
	public void test_3_1() {
		get("http://localhost:" + port + "/downloadTest/getFile_3_1").then().statusCode(200);
	}

	@Test
	public void test_3_2() {
		get("http://localhost:" + port + "/downloadTest/getFile_3_2").then().statusCode(200);
	}

	@Test
	public void test_3_3() {
		get("http://localhost:" + port + "/downloadTest/getFile_3_3").then().statusCode(200);
	}

	@Test
	public void test_4_0() {
		get("http://localhost:" + port + "/downloadTest/getFile_4_0").then().statusCode(200);
		// TODO works, but needs treatment on the receiving end
	}

	@Test
	public void test_4_1() {
		get("http://localhost:" + port + "/downloadTest/getFile_4_1").then().statusCode(200);
		// TODO untested
	}

	@Test
	public void test_4_2() {
		get("http://localhost:" + port + "/downloadTest/getFile_4_2").then().statusCode(200);
		// TODO untested
	}

	@Test
	public void test_4_3() {
		get("http://localhost:" + port + "/downloadTest/getFile_4_3").then().statusCode(200);
		// TODO untested
	}

	@Test
	public void test_5_0() {
		get("http://localhost:" + port + "/downloadTest/getFile_5_0").then().statusCode(200);
		// TODO doesn't actually work
	}

	@Test
	public void test_5_1() {
		get("http://localhost:" + port + "/downloadTest/getFile_5_1").then().statusCode(200);
		// TODO doesn't actually work
	}

	@Test
	public void test_5_2() {
		get("http://localhost:" + port + "/downloadTest/getFile_5_2").then().statusCode(200);
		// TODO doesn't actually work
	}

	@Test
	public void test_5_3() {
		get("http://localhost:" + port + "/downloadTest/getFile_5_3").then().statusCode(200);
		// TODO doesn't actually work
	}

	@Test
	public void test_6_0() {
		get("http://localhost:" + port + "/downloadTest/getFile_6_0").then().statusCode(200);
	}

	@Test
	public void test_6_1() {
		get("http://localhost:" + port + "/downloadTest/getFile_6_1").then().statusCode(200);
	}

	@Test
	public void test_6_2() {
		get("http://localhost:" + port + "/downloadTest/getFile_6_2").then().statusCode(200);
	}

	@Test
	public void test_6_3() {
		get("http://localhost:" + port + "/downloadTest/getFile_6_3").then().statusCode(200);
	}

	@Test
	public void test_7_0() {
		get("http://localhost:" + port + "/downloadTest/getFile_7_0").then().statusCode(200);
	}

	@Test
	public void test_7_1() {
		get("http://localhost:" + port + "/downloadTest/getFile_7_1").then().statusCode(200);
	}

	@Test
	public void test_7_2() {
		get("http://localhost:" + port + "/downloadTest/getFile_7_2").then().statusCode(200);
	}

	@Test
	public void test_7_3() {
		get("http://localhost:" + port + "/downloadTest/getFile_7_3").then().statusCode(200);
	}
}
