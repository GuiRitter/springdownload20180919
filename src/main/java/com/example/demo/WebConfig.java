package com.example.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DelegatingWebMvcConfiguration;

@Configuration
public class WebConfig extends DelegatingWebMvcConfiguration {

//	/**
//	 * https://stackoverflow.com/questions/32351142/httpmediatypenotacceptableexception-could-not-find-acceptable-representation-in
//	 */
//	@Override
//	protected void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
//		configurer.favorParameter(false);
//		configurer.favorPathExtension(false);
//		super.configureContentNegotiation(configurer);
//	}

//	/**
//	 * https://stackoverflow.com/questions/34182523/spring-rest-web-service-return-file-as-resource
//	 * https://stackoverflow.com/questions/32351142/httpmediatypenotacceptableexception-could-not-find-acceptable-representation-in
//	 */
//	@Override
//	protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//	    MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
//
//	    ObjectMapper objectMapper = new ObjectMapper();
//	    objectMapper.registerModule(new Hibernate5Module());
//	    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//	    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
//	    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);//
//
//	    messageConverter.setObjectMapper(objectMapper);
//	    messageConverter.setSupportedMediaTypes(Arrays.asList(
//	    		MediaType.APPLICATION_JSON,
//	    		MediaType.APPLICATION_JSON_UTF8,
//	    		MediaType.APPLICATION_OCTET_STREAM,
//	    		MediaType.APPLICATION_PROBLEM_JSON,
//	    		MediaType.APPLICATION_PROBLEM_JSON_UTF8,
//	    		MediaType.APPLICATION_STREAM_JSON,
//	    		MediaType.TEXT_EVENT_STREAM,
//	    		MediaType.TEXT_HTML,
//	    		MediaType.TEXT_MARKDOWN,
//	    		MediaType.TEXT_PLAIN,
//	    		MediaType.TEXT_XML
//	    		));//
//
//	    ByteArrayHttpMessageConverter arrayHttpMessageConverter = new ByteArrayHttpMessageConverter();//
//	    arrayHttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));//
//
//	    converters.add(messageConverter);
//	    converters.add(arrayHttpMessageConverter);//
//	    converters.add(new ResourceHttpMessageConverter());
//		super.configureMessageConverters(converters);
//	}
}
