package com.example.demo;

import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.readAllBytes;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@RepositoryRestController()
@RequestMapping("downloadTest")
public class Controller {

	@Value("${downloadFile}")
	private String downloadFile = "download.jpg";

	@Value("${downloadPath}")
	private String downloadPath = "C:/Users/ampix/Desktop/";

	private HttpHeaders getCommonSetup(HttpServletResponse response, int length) {
		response.setContentType(APPLICATION_OCTET_STREAM_VALUE);
		response.setHeader("Content-Disposition", "attachment; filename=" + downloadFile);
		response.setHeader("Content-Length", length + "");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(APPLICATION_OCTET_STREAM);
		headers.setContentDisposition(ContentDisposition.parse("attachment; filename=" + downloadFile));
		headers.setContentLength(length);
		return headers;
	}

	private ResponseEntity<byte[]> getFile_0(HttpServletResponse response) throws IOException {
		byte[] contents = readAllBytes(Paths.get(downloadPath + downloadFile));
		return new ResponseEntity<byte[]>(contents, getCommonSetup(response, contents.length), OK);
	}

	@RequestMapping(method = GET, value = "getFile_0_0")
	public ResponseEntity<byte[]> getFile_0_0(HttpServletResponse response) throws IOException {
		return getFile_0(response);
	}

	@RequestMapping(method = GET, value = "getFile_0_1", produces = APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> getFile_0_1(HttpServletResponse response) throws IOException {
		return getFile_0(response);
	}

	@RequestMapping(method = GET, value = "getFile_0_2")
	@ResponseBody
	public ResponseEntity<byte[]> getFile_0_2(HttpServletResponse response) throws IOException {
		return getFile_0(response);
	}

	@RequestMapping(method = GET, value = "getFile_0_3", produces = APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public ResponseEntity<byte[]> getFile_0_3(HttpServletResponse response) throws IOException {
		return getFile_0(response);
	}

	public ResponseEntity<Resource> getFile_1(HttpServletResponse response) throws IOException {
		Path path = Paths.get(downloadPath + downloadFile);
		return new ResponseEntity<Resource>(
				new InputStreamResource(newInputStream(path)),
				getCommonSetup(response, (int) path.toFile().length()),
				OK);
	}

	@RequestMapping(method = GET, value = "getFile_1_0")
	public ResponseEntity<Resource> getFile_1_0(HttpServletResponse response) throws IOException {
		return getFile_1(response);
	}

	@RequestMapping(method = GET, value = "getFile_1_1", produces = APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Resource> getFile_1_1(HttpServletResponse response) throws IOException {
		return getFile_1(response);
	}

	@RequestMapping(method = GET, value = "getFile_1_2")
	@ResponseBody
	public ResponseEntity<Resource> getFile_1_2(HttpServletResponse response) throws IOException {
		return getFile_1(response);
	}

	@RequestMapping(method = GET, value = "getFile_1_3", produces = APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public ResponseEntity<Resource> getFile_1_3(HttpServletResponse response) throws IOException {
		return getFile_1(response);
	}

	public HttpEntity<byte[]> getFile_2(HttpServletResponse response) throws IOException {
		byte[] contents = readAllBytes(Paths.get(downloadPath + downloadFile));
	    return new HttpEntity<byte[]>(contents, getCommonSetup(response, contents.length));
	}

	@RequestMapping(method = GET, value = "getFile_2_0")
	public HttpEntity<byte[]> getFile_2_0(HttpServletResponse response) throws IOException {
		return getFile_2(response);
	}

	@RequestMapping(method = GET, value = "getFile_2_1", produces = APPLICATION_OCTET_STREAM_VALUE)
	public HttpEntity<byte[]> getFile_2_1(HttpServletResponse response) throws IOException {
		return getFile_2(response);
	}

	@RequestMapping(method = GET, value = "getFile_2_2")
	@ResponseBody
	public HttpEntity<byte[]> getFile_2_2(HttpServletResponse response) throws IOException {
		return getFile_2(response);
	}

	@RequestMapping(method = GET, value = "getFile_2_3", produces = APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public HttpEntity<byte[]> getFile_2_3(HttpServletResponse response) throws IOException {
		return getFile_2(response);
	}

	public ResponseEntity<InputStreamResource> getFile_3(HttpServletResponse response) throws IOException {
	    File file = new File(downloadPath + downloadFile);
	    return new ResponseEntity<InputStreamResource>(
	    		new InputStreamResource(new FileInputStream(file)),
	    		getCommonSetup(response, (int) file.length()),
	    		OK);
	}

	@RequestMapping(method = GET, value = "getFile_3_0")
	public ResponseEntity<InputStreamResource> getFile_3_0(HttpServletResponse response) throws IOException {
		return getFile_3(response);
	}

	@RequestMapping(method = GET, value = "getFile_3_1", produces = APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<InputStreamResource> getFile_3_1(HttpServletResponse response) throws IOException {
		return getFile_3(response);
	}

	@RequestMapping(method = GET, value = "getFile_3_2")
	@ResponseBody
	public ResponseEntity<InputStreamResource> getFile_3_2(HttpServletResponse response) throws IOException {
		return getFile_3(response);
	}

	@RequestMapping(method = GET, value = "getFile_3_3", produces = APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public ResponseEntity<InputStreamResource> getFile_3_3(HttpServletResponse response) throws IOException {
		return getFile_3(response);
	}

	public StreamingResponseBody getFile_4(HttpServletResponse response) throws IOException {
	    File file = new File(downloadPath + downloadFile);
		getCommonSetup(response, (int) file.length());
		InputStream inputStream = new FileInputStream(file);
		return outputStream -> {
			int nRead;
			byte[] data = new byte[1024];
			while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
				outputStream.write(data, 0, nRead);
			}
			inputStream.close();
		};
	}

	@RequestMapping(method = GET, value = "getFile_4_0")
	public StreamingResponseBody getFile_4_0(HttpServletResponse response) throws IOException {
		return getFile_4(response);
	}

	@RequestMapping(method = GET, value = "getFile_4_1", produces = APPLICATION_OCTET_STREAM_VALUE)
	public StreamingResponseBody getFile_4_1(HttpServletResponse response) throws IOException {
		return getFile_4(response);
	}

	@RequestMapping(method = GET, value = "getFile_4_2")
	@ResponseBody
	public StreamingResponseBody getFile_4_2(HttpServletResponse response) throws IOException {
		return getFile_4(response);
	}

	@RequestMapping(method = GET, value = "getFile_4_3", produces = APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public StreamingResponseBody getFile_4_3(HttpServletResponse response) throws IOException {
		return getFile_4(response);
	}

	public ResponseEntity<StreamingResponseBody> getFile_5(HttpServletResponse response) throws IOException {
	    File file = new File(downloadPath + downloadFile);
		InputStream inputStream = new FileInputStream(file);
		return ResponseEntity.ok().headers(getCommonSetup(response, (int) file.length())).body(outputStream -> {
			int nRead;
			byte[] data = new byte[1024];
			while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
				outputStream.write(data, 0, nRead);
			}
			inputStream.close();
		});
	}

	@RequestMapping(method = GET, value = "getFile_5_0")
	public ResponseEntity<StreamingResponseBody> getFile_5_0(HttpServletResponse response) throws IOException {
		return getFile_5(response);
	}

	@RequestMapping(method = GET, value = "getFile_5_1", produces = APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<StreamingResponseBody> getFile_5_1(HttpServletResponse response) throws IOException {
		return getFile_5(response);
	}

	@RequestMapping(method = GET, value = "getFile_5_2")
	@ResponseBody
	public ResponseEntity<StreamingResponseBody> getFile_5_2(HttpServletResponse response) throws IOException {
		return getFile_5(response);
	}

	@RequestMapping(method = GET, value = "getFile_5_3", produces = APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public ResponseEntity<StreamingResponseBody> getFile_5_3(HttpServletResponse response) throws IOException {
		return getFile_5(response);
	}

	public ResponseEntity<Resource> getFile_6(HttpServletResponse response) throws IOException {
	    Path path = Paths.get(downloadPath + downloadFile);
	    ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
	    return ResponseEntity.ok()
	            .headers(getCommonSetup(response, (int) path.toFile().length()))
	            .contentLength(path.toFile().length())
	            .contentType(APPLICATION_OCTET_STREAM)
	            .body(resource);
	}

	@RequestMapping(method = GET, value = "getFile_6_0")
	public ResponseEntity<Resource> getFile_6_0(HttpServletResponse response) throws IOException {
		return getFile_6(response);
	}

	@RequestMapping(method = GET, value = "getFile_6_1", produces = APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Resource> getFile_6_1(HttpServletResponse response) throws IOException {
		return getFile_6(response);
	}

	@RequestMapping(method = GET, value = "getFile_6_2")
	@ResponseBody
	public ResponseEntity<Resource> getFile_6_2(HttpServletResponse response) throws IOException {
		return getFile_6(response);
	}

	@RequestMapping(method = GET, value = "getFile_6_3", produces = APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public ResponseEntity<Resource> getFile_6_3(HttpServletResponse response) throws IOException {
		return getFile_6(response);
	}

	public ModelAndView getFile_7(HttpServletResponse response) throws IOException {
        URL url = new URL(downloadPath + downloadFile);
	    getCommonSetup(response, (int) Paths.get(downloadPath + downloadFile).toFile().length());
        InputStream is = url.openStream();
        BufferedOutputStream outs = new BufferedOutputStream(response.getOutputStream());
        int len;
        byte[] buf = new byte[1024];
        while ( (len = is.read(buf)) > 0 ) {
            outs.write(buf, 0, len);
        }
        outs.close();
        return null;
	}

	@RequestMapping(method = GET, value = "getFile_7_0")
	public ModelAndView getFile_7_0(HttpServletResponse response) throws IOException {
		return getFile_7(response);
	}

	@RequestMapping(method = GET, value = "getFile_7_1", produces = APPLICATION_OCTET_STREAM_VALUE)
	public ModelAndView getFile_7_1(HttpServletResponse response) throws IOException {
		return getFile_7(response);
	}

	@RequestMapping(method = GET, value = "getFile_7_2")
	@ResponseBody
	public ModelAndView getFile_7_2(HttpServletResponse response) throws IOException {
		return getFile_7(response);
	}

	@RequestMapping(method = GET, value = "getFile_7_3", produces = APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public ModelAndView getFile_7_3(HttpServletResponse response) throws IOException {
		return getFile_7(response);
	}
}
